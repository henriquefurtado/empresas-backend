module.exports = (sequelize, DataTypes) => {
  const Token = sequelize.define('Token', {
    token: DataTypes.STRING,
    user_id: DataTypes.INTEGER,
  })

  Token.associate = models => {
    models.Token.belongsTo(models.User)
  }

  return Token
}
