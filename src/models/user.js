module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: DataTypes.STRING,
    password: DataTypes.STRING,
  })

  User.associate = models => {
    models.User.hasMany(models.Token, {
      throught: 'token',
      as: 'token',
      foreignKey: 'user_id',
    })
  }

  return User
}
