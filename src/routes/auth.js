const router = require('express').Router()
const AuthController = require('../controllers/AuthController')
const isAuthorized = require('../middlewares/isAuthorized')

router.post('/login', AuthController.authenticate)
router.delete('/logout', isAuthorized, AuthController.logout)

module.exports = router
