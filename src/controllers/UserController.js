const { User } = require('../models')
const bcrypt = require('bcrypt')
const {
  errorHandler: ApplicationError,
  email: { formatEmail },
} = require('../helpers')

module.exports = {
  create: async (req, res) => {
    const { password } = req.body
    let { email } = req.body

    try {
      if (email) {
        email = formatEmail(email)
      }

      const userExist = await User.findOne({ where: { email } })
      if (userExist) {
        throw new ApplicationError('Usuário ja existe', 400)
      }

      var hashedPassword = bcrypt.hashSync(password, 8)
      let user = await User.create({
        email,
        password: hashedPassword,
      })
      return res.json(user)
    } catch (error) {
      const { status, message } = error
      res.status(status || 500).json({
        errors: [message],
      })
    }
  },

  delete: async (req, res) => {
    try {
      const { id } = req.params
      await User.destroy({ where: { id } })
      return res.status(204)
    } catch (error) {
      console.log(error)
    }
  },

  update: async (req, res) => {
    try {
      const user_id = await req.params.id
      const { email, password } = req.body
      User.update(
        {
          email: email,
          password: password,
        },
        {
          where: {
            id: user_id,
          },
        }
      )
      return res.status(204)
    } catch (error) {
      console.log(error)
    }
  },

  getAll: async (req, res) => {
    try {
      const user = await User.findAll()
      return res.json(user)
    } catch (error) {
      console.log(error)
    }
  },

  get: async (req, res) => {
    try {
      const user = await User.findOne({ where: { id: req.params.id } })

      if (user) return res.json(user)
      else throw new ApplicationError('Usuário não encontrado', 404)
    } catch (error) {
      const { status, message } = error
      res.status(status || 500).json({
        errors: [message],
      })
    }
  },

  updateCurrent: async (req, res) => {
    try {
      let { email, password } = req.body
      var hashedPassword = bcrypt.hashSync(password, 8)
      const user = await User.update(
        {
          email: email,
          password: hashedPassword,
        },
        {
          where: {
            id: req.session.user.id,
          },
        }
      )
      return res.json({ email, password })
    } catch (error) {
      console.log(error)
    }
  },
}
