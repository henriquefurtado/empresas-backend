const { Company } = require('../models')
const { Op } = require('sequelize')
const { errorHandler: ApplicationError } = require('../helpers')

module.exports = {
  create: async (req, res) => {
    try {
      const { company_name, company_type } = req.body

      const company = await Company.create({
        company_name,
        company_type,
      })
      return res.json(company)
    } catch (e) {
      console.log(e)
    }
  },
  delete: async (req, res) => {
    try {
      const { id } = req.params
      const companyExist = await Company.findOne({ where: { id } })

      if (!companyExist) {
        throw new ApplicationError('Empresa não encontrada', 404)
      }

      await Company.destroy({ where: { id } })

      res.status(204)
    } catch (error) {
      const { status, message } = error
      res.status(status || 500).json({
        errors: [message],
      })
    }
  },

  getAll: async (req, res) => {
    let { company_name, company_type } = req.query
    if (company_type === undefined && company_name === undefined) {
      company_name = ''
      company_type = ''
    }
    try {
      const company = await Company.findAll({
        where: {
          [Op.or]: [
            {
              company_name: {
                [Op.iLike]: `%${company_name}%`,
              },
            },
            {
              company_type: {
                [Op.iLike]: `%${company_type}%`,
              },
            },
          ],
        },
      })
      return res.json(company)
    } catch (e) {
      console.log(e)
    }
  },

  get: async (req, res) => {
    try {
      const company = await Company.findOne({ where: { id: req.params.id } })

      if (company) return res.json(company)
      else throw new ApplicationError('Empresa não encontrada', 404)
    } catch (error) {
      const { status, message } = error
      res.status(status || 500).json({
        errors: [message],
      })
    }
  },
  update: async (req, res) => {
    try {
      const company_id = req.params.id
      const { company_name, company_type } = req.body
      Company.update(
        {
          company_name: company_name,
          company_type: company_type,
        },
        {
          where: {
            id: company_id,
          },
        }
      )
      return res.json({ success: 'Atualizado com sucesso' })
    } catch (e) {
      console.log(e)
    }
  },
}
