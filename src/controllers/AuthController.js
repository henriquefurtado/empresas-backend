const { User, Token } = require('../models')
const { getToken } = require('../helpers/authorize')
const { encryptor, errorHandler: ApplicationError } = require('../helpers')
module.exports = {
  authenticate: async (req, res) => {
    try {
      const { email, password } = req.body

      if (!email || !password) {
        throw new ApplicationError('Email ou senha faltando', 403)
      }

      const user = await User.findOne({ where: { email } })
      if (!user) {
        throw new ApplicationError('Usuário não encontrado', 404)
      }

      if (!encryptor.comparePassword(password, user.password)) {
        throw new ApplicationError('Senha inválida', 400)
      }

      const token = encryptor.generateToken({
        id: user.id,
        email: user.email,
      })

      await Token.create({
        token,
        user_id: user.id,
      })

      return res.json({
        email,
        token,
      })
    } catch (error) {
      const { status, message } = error
      res.status(status || 500).json({
        errors: [message],
      })
    }
  },
  logout: async (req, res) => {
    try {
      const token = getToken(req, res)
      await Token.destroy({ where: { token, user_id: req.session.user.id } })
      req.session = {}
      return res.status(204)
    } catch (error) {
      console.log(error)
    }
  },
}
