const { errorHandler: ApplicationError } = require('../helpers')

module.exports = {
  getToken: (req, res) => {
    try {
      if (req.headers && req.headers.authorization) {
        const parts = req.headers.authorization.split(' ')

        if (parts.length === 2) {
          const scheme = parts[0]
          const credentials = parts[1]

          if (/^Bearer$/i.test(scheme)) {
            return credentials
          } else {
            throw new ApplicationError('Erro ao autorizar', 403)
          }
        }
      } else {
        throw new ApplicationError('Erro ao autorizar', 403)
      }
    } catch (e) {
      const { status, name, message } = new ApplicationError(e)
      res.status(status || 500).json({
        name,
        message,
      })
    }
  },
}
