module.exports = {
  formatEmail: email => email.replace(/\s+/g, '').toLowerCase(),
}
