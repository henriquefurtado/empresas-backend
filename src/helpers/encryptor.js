const {
  [process.env.NODE_ENV]: { secret },
} = require('../config/env')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

module.exports = {
  hashPassword: password => bcrypt.hashSync(password),
  comparePassword: (password, userPassword) =>
    bcrypt.compareSync(password, userPassword),

  generateToken: user => {
    return jwt.sign(user, secret)
  },

  verifyToken: user => {
    return jwt.verify(token, 'wrong-secret')
  },

  generateRandString: () =>
    Math.random()
      .toString(36)
      .substring(2, 15),
}
