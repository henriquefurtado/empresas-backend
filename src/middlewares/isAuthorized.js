const { promisify } = require('util')
const jwt = require('../helpers/jwt')
const { User, Token } = require('../models')
const { errorHandler: ApplicationError } = require('../helpers')

const verify = promisify(jwt.verify)

module.exports = async (req, res, next) => {
  let token
  try {
    if (req.headers && req.headers.authorization) {
      const parts = req.headers.authorization.split(' ')

      if (parts.length === 2) {
        const scheme = parts[0]
        const credentials = parts[1]

        if (/^Bearer$/i.test(scheme)) {
          token = credentials
        } else {
          throw new ApplicationError('Erro ao autorizar', 403)
        }
      }
    } else {
      throw new ApplicationError('Erro ao autorizar', 403)
    }
    const { id, email } = await verify(token)

    const tokenExist = await Token.findOne({ where: { token } })
    if (!tokenExist) {
      throw new ApplicationError('Token não encontrado', 404)
    }

    const user = await User.findOne({ where: { id, email } })
    if (!user) {
      throw new ApplicationError('Usuário não encontrado', 404)
    }

    if (id !== user.id) {
      throw new ApplicationError('Usuário não autorizado', 404)
    }

    req.session = {
      token,
      user,
    }

    next()
  } catch (error) {
    const { status, message } = error
    res.status(status || 500).json({
      errors: [message],
    })
  }
}
